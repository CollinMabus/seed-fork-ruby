#README NOTES


db demo error:
$ createuser --createdb --login -P demo
enter 'demo' as password...

rails_app_development dne error:
$ bundle exec rails db:create
$ psql
--> look at video: goo.gl/UkrqJM at 14:40

migrate error:
$ bundle exec rails db:migrate




##START THE APPLICATION:
    $ cd workspace/seedfork
    ## The command below must be written in a separate terminal from the command after it.
    $ sudo service postgresql start
    $ bundle exec rails server -b $IP -p $PORT

##ADD PAGES:
- Save File: `/views/dashboard/page1.html.erb`  
- Create Link: `/config/routes.rb [get 'dashboard/page1']`  
- Add PAGE: `/app/views/layouts/application.html.erb`  
- HTML Link: `<li><%= link_to "Page1", dashboard_PAGE_path %></li>`  
- Database Link: `<li><%= link_to "Page2", DB_path %></li>`  
- Database Index: `app/views/DBNAME/index.html.erb` 

##ADD POSTGRESQL DATABASE
####STEP1: Create database: Data Types --> "http://www.w3resource.com/PostgreSQL/data-types.php" 
    $ rails generate scaffold TABLENAME COL1:string COL2:decimal COL3:datatype ...  
    $ rails g scaffold donor username:string date_entered:date email:string user_type:string ...
      quantity:integer address:string season:string match:string pickup_date:date pickup_name:string
    $ rails g scaffold recipient username:string date_entered:date email:string user_type:string ...
      quantity:integer address:string match:string dropoff_date:date dropoff_name:string
    $ rails g migration add_weight_to_donors weight:int
####STEP2: Migrate to add new tables to database  
    $ rails db:migrate [Creates table in database]  
- Database Index: `app/views/DBNAME/index.html.erb`
- Databse Add Item: `app/views/DBNAME/_form.html.erb`
- Show Single Item: `app/views/DBNAME/show.html.erb`
- NOTE: DO NOT name a column "type" took me forever to fix the database...

####Change Column Name  
    $ rails g migration change_col_name
    db/migrate/change_col_name: change_column :my_table, :my_column, :my_new_type

####PSQL
    $ psql
    # \l [List Databases]
    # \c rails_app_development [Switch Databases]
    # \dt [List Tables]
    # \d [Describe a Table]
    # \d+ <tablename> [Shows Tables Columns]
    # \dn [List Schema]
    # \dv [List Views]
    # \du [List Users and their Roles]
    # \s [Save Command History to a File]
    # \s filename [Execute PSQL Commands from a File]
    # \? [HELP]
    # \h ALTER TABLE [Help on Specific Statement]
    # \q [Exits PSQL]
    
##PAGE STORAGE:
- Store Test Files/Code: `app/storage`
- $ history [shows terminals command history]

##PUSH TO BITBUCKET USING GIT
####INITIAL CONNECT AND PUSH TO BITBUCKET:
    $ git init
    $ git remote remove origin
    $ git remote add origin git@bitbucket.org:CocoaMuffs/seed-fork-ruby.git
    $ git add *
    $ git add .
    $ git status
    $ git commit -am "Comment"
    $ git push -u origin master
####*UPDATE MASTER:
    $ git init
    $ git remote remove origin
    $ git remote add origin git@bitbucket.org:CocoaMuffs/seed-fork-ruby.git
    $ git pull git@bitbucket.org:CocoaMuffs/seed-fork-ruby.git
    * Make Changes...
    $ git add .
    $ git status
    $ git commit -am "Comment"
    $ git merge master
    $ git push -u origin master
    
    
##IMPLEMENTING DEVISE (user login system)
    $ bundle exec rails generate devise:install
    $ bundle exec rails generate devise users
- Users Location: `app/db/migrate/..._devise_create_users.rb`

##REFERENCE
    https://confluence.atlassian.com/bitbucket/branching-a-repository-223217999.html
    REVERT (amazing): c9/File/Show File Revision History

##TO-DO
    [x] Database pages need to have a COMMON nav-bar for code-reuse (modal)
    [] Automatically add username, user_type, email, etc to database insertions,
         but do not show them in the form when entering new row
    [x] Create a standard non-user database main screen showing no personal information,
         which also shows matches to pantries, without showing who produce is coming from (for security)
    [] Clean up db/schema.rb
    [x] User Accounts Updatable
    [x] User Locked Screens
    [] Error in donors_controller.rb -- I dont know why it is throwing it when viewing individual farms
    [] Update Form Styling