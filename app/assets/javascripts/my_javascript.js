//USING THIS TO MAKE MODIFICATIONS TO CODE TAHT WILL BE RUN WHEN APPLICATION IS LOADED

//EXPLAINATION ICONS ON THE LEFT OF THE LINE NUMBERS MADE ME ADD FOLLOWING TWO LINES
/*global $*/
/*global i*/
/*global j*/
/*global jQuery*/
/*global $t*/


//JQUERY - WHATEVER THE DOCUMENT OBJECT IS, WE WAIT UNTIL THE PAGE IS FULLY RENDERED AND THEN WE RUN THESE COMMANDS
$('document').ready(function(){
  
  /*=========================================*/
  /* DONOR MODAL - SHOWS PHONE/EMAIL/ADDRESS */
  /*=========================================*/
  if($('#donorList').length){
    var myDonList = document.getElementById("donorList").getElementsByTagName("tr");
    for(var i = 0; i < myDonList.length; i++){
      myDonList[i].addEventListener("click", activateItem);
    }
  }
  
  /*======================*/
  /*    MODAL FUNCTION    */
  /*======================*/
  function activateItem(){
    //alert("hello");
    //alert(this.innerHTML); //shows entire html for that row
    //NOTE: Count Column Names, Along With Values
    var person = this.childNodes[1].innerHTML;
    var phone = this.childNodes[15].innerHTML;
    var email = this.childNodes[17].innerHTML;
    var address = this.childNodes[19].innerHTML;
    
    var multiStr = [
      "Phone: " + phone,
      "Email: " + email,
      "Address: " + address
    ].join('<br />');
    
    document.getElementById("donModalLabel").innerHTML = person;
    document.getElementById("donModalBody").innerHTML = multiStr;
  }
  
  /*==================================*/
  /* SELECT FARMER - DONOR/INDEX.html */
  /*==================================*/
  $(function(){
    $(document).on('click', '.dropdown-menu li a', function() {
      var selected_farmer = $(this).html(); //alert(selected_farmer);
      document.getElementById("nav_farmer").innerHTML = selected_farmer;
      document.getElementById("search").value = selected_farmer;
      document.getElementById("nav_farmer").style.padding = "16px";
      
      //var farmerList = document.getElementById("donorList").getElementsByTagName("tr");
      var bool = "false";
      
      var table = document.getElementById('donorList');
      for (var r = 0, n = table.rows.length; r < n; r++) {
          //Loop through each cell in the table
          for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
              var row_farmer = table.rows[r].cells[0].innerHTML;
              $t = $(this);
              //alert(row_farmer);
              if (row_farmer == selected_farmer) {
                //alert("SHOW ROW: " + r);
                  table.rows[r].cells[c].style.color='black';
                  $t.show();
                  bool = "true";
              } else {
                if (selected_farmer == "All Farmers") {
                  //alert("SHOW ALL: " + r);
                  table.rows[r].cells[c].style.color='black';
                  $t.show();
                  bool = "true";
                } else {
                  //alert("HIDE ROW: " + r);
                  table.rows[r].cells[c].style.color='white';
                  $t.hide();
                  //table.rows[r].cells[0].innerHTML="REMOVE";
                }
              }
          }
      }
      if (bool == "false") {
        alert("No results found for " + selected_farmer);
        for (var r = 0, n = table.rows.length; r < n; r++) {
          for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
            table.rows[r].cells[c].style.color='black'; //SHOW ALL ROWS
          }
        }
        document.getElementById("nav_farmer").innerHTML = "All Farmers";
      }
    });
  });
  

  /*==========================================*/
  /* SEARCH BY DATE - DASHBOARD/ARCHIVES.html */
  /*==========================================*/
  jQuery(document).ready(function(){
    jQuery('#hideshow').on('click', function(event) {
      jQuery('#content').toggle('show');
    });
  });
  
  $(function(){
    $(document).on('click', '#get_results', function() {
      var selected_start = document.getElementById("start_date").value;
      var selected_end = document.getElementById("end_date").value;
      
      var table = document.getElementById('donorList');
      
      for (var row = 0, n = table.rows.length; row < n; row++) {
          //var value = formatDate(table.rows[row].cells[11].innerHTML);
          //[]NEED TO CONVERT DATE TO OTHER FORMAT
          //var dt = new Date(value);
          //var formatedDate = dt.format("yyyy-mm-dd");
          //alert("Current Date: " + value);
          //alert("Formated Date: " + formatedDate);
          
          for (var col = 0, m = table.rows[row].cells.length; col < m; col++) {
              var row_date = formatDate(table.rows[row].cells[11].innerHTML);
              if (row_date >= selected_start && row_date <= selected_end) {
                  table.rows[row].cells[col].style.color='black';
              } else {
                table.rows[row].cells[col].style.color='white';
                
              }
          }
      }
    });
  });
  
  /*============================================*/
  /* CONVERT DATE - CONVERTS DATE TO MM/DD/YYYY */
  /*============================================*/
  function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [month, day, year].join('/');
  }
  
  /*==========================================*/
  /* CLEAR RESULTS - DASHBOARD/ARCHIVES.html  */
  /*==========================================*/
  $(function(){
    $(document).on('click', '#clear_results', function() {
      var table = document.getElementById('donorList');
      document.getElementById('start_date').value = "";
      document.getElementById('end_date').value = "";
      for (var rr = 0, n = table.rows.length; rr < n; rr++) {
        for (var cc = 0, m = table.rows[rr].cells.length; cc < m; cc++) {
          table.rows[rr].cells[cc].style.color='black'; //SHOW ALL ROWS
        }
      }
    });
  });
  
  /*======================================*/
  /* SEARCH BY COLUMN - DONORS/INDEX.html */
  /*======================================*/
  $("#search").on("keyup", function() {
    var value = $(this).val();
    var str = document.getElementById("search").value;
    document.getElementById("search").value = str.charAt(0).toUpperCase() + str.slice(1);
    
    $("table tr").each(function(index) {
      if (index !== 0) {
        //alert("Index: " + index);
        var id = $(this).find("td:first").text();
        
        if (id.indexOf(value) !== 0) {
          $(this).hide();
        } else {
          $(this).show();
        }
      }
    });
  });
  
});