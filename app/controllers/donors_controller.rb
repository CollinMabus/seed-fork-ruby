class DonorsController < ApplicationController
  
  before_action :authenticate_user!
  before_action :set_donor, only: [:show, :edit, :update, :destroy]
  
  # GET /donors
  # GET /donors.json
  def index
    @donors = Donor.all.order(:username, :type_of_food)
    @recipients = Recipient.order(:username)
    @usernames = Donor.select(:username).distinct.order(:username)
    @farmer = Donor.where("delivered = 'no' and pickup_name IS NULL").order(:username, :type_of_food)
   
    #@search_farmers = Donor.search(params[:search]).order(sort_column + " " + sort_direction)
  end
 
  
  # GET /donors/1
  # GET /donors/1.json
  def show
    #@donors = Donor.find(params[:id])
    #@donors = Donor.all
  end
  
  # GET /donors/new
  def new
    @donor = Donor.new
  end
  
  # GET /donors/1/edit
  def edit
  end
  
  # POST /donors
  # POST /donors.json
  def create
    @donor = Donor.new(donor_params)

    respond_to do |format|
      if @donor.save
        #format.html { redirect_to @donor, notice: 'Donor was successfully created.' }
        format.html { redirect_to donors_url, notice: 'Produce was successfully created.' }
        format.json { render :show, status: :created, location: @donor }
      else
        format.html { render :new }
        format.json { render json: @donor.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /donors/1
  # PATCH/PUT /donors/1.json
  def update
    respond_to do |format|
      if @donor.update(donor_params)
        #format.html { redirect_to @donor, notice: 'Donor was successfully updated.' }
        format.html { redirect_to donors_url, notice: 'Produce was successfully updated.' }
        format.json { render :show, status: :ok, location: @donor }
      else
        format.html { render :edit }
        format.json { render json: @donor.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /donors/1
  # DELETE /donors/1.json
  def destroy
    @donor.destroy
    respond_to do |format|
      format.html { redirect_to donors_url, notice: 'Produce was successfully Deleted.' }
      format.json { head :no_content }
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_donor
      #[] I DONT KNOW WHY THIS IS THROWING AN ERROR: "Couldn't find Donor with 'id'=waltersfarm"
      @donor = Donor.find(params[:id])
      #@donor = Donor.find(params[:username])
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def donor_params
      params.require(:donor).permit(:username, :date_entered, :type_of_food, :price, :quantity, :weight, :units, :pickup_name, :pickup_date, :match, :email, :phone, :user_type, :quantity, :address, :season, :delivered)
    end
    
end