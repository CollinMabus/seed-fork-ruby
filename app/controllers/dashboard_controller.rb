class DashboardController < ApplicationController
  def index
  end
  
  def truck_inventory
    @usernames = Donor.select(:username).distinct.order(:username)
    #Example: @honors = Honor.where("owner = ?", current_user.id)
    #@inventory = Donor.where("pickup_date != ?", nil)
    #@inventory = Donor.where("pickup_date != ?", "1000-01-01")
    @inventory = Donor.where("pickup_date != ? and delivered = 'no'", "1000-01-01").order(:pickup_date)
    @waltersfarm = Donor.where("username = ?", "Walters Farm").order(:date_entered, :type_of_food)
    #@inventory = Donor.exists? pickup_date: !nil
    #SQL: SELECT * FROM "donors" WHERE (pickup_date != NULL), RECIPIENTS.dropoff_date=null;
    #@search = Donor.where("username = ?", selected_farmer).order(:type_of_food)
  end
  
  def archives
    @usernames = Donor.select(:username).distinct.order(:username)
    @archived = Donor.where("pickup_date != ? and delivered = 'yes'", "1000-01-01").order(:pickup_date)
  end
  
  def matches
    @usernames = Donor.select(:username).distinct.order(:username)
    @matches = Donor.where("pickup_date != ?", "1000-01-01").order(:pickup_date)
  end

  def page1
  end

  def page2
  end
  
end