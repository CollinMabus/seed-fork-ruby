class SeedForkUsersController < ApplicationController
  before_action :set_seed_fork_user, only: [:show, :edit, :update, :destroy]

  # GET /seed_fork_users
  # GET /seed_fork_users.json
  def index
    @seed_fork_users = SeedForkUser.all
  end

  # GET /seed_fork_users/1
  # GET /seed_fork_users/1.json
  def show
  end

  # GET /seed_fork_users/new
  def new
    @seed_fork_user = SeedForkUser.new
  end

  # GET /seed_fork_users/1/edit
  def edit
  end

  # POST /seed_fork_users
  # POST /seed_fork_users.json
  def create
    @seed_fork_user = SeedForkUser.new(seed_fork_user_params)

    respond_to do |format|
      if @seed_fork_user.save
        format.html { redirect_to @seed_fork_user, notice: 'Seed fork user was successfully created.' }
        format.json { render :show, status: :created, location: @seed_fork_user }
      else
        format.html { render :new }
        format.json { render json: @seed_fork_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /seed_fork_users/1
  # PATCH/PUT /seed_fork_users/1.json
  def update
    respond_to do |format|
      if @seed_fork_user.update(seed_fork_user_params)
        format.html { redirect_to @seed_fork_user, notice: 'Seed fork user was successfully updated.' }
        format.json { render :show, status: :ok, location: @seed_fork_user }
      else
        format.html { render :edit }
        format.json { render json: @seed_fork_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /seed_fork_users/1
  # DELETE /seed_fork_users/1.json
  def destroy
    @seed_fork_user.destroy
    respond_to do |format|
      format.html { redirect_to seed_fork_users_url, notice: 'Seed fork user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_seed_fork_user
      @seed_fork_user = SeedForkUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def seed_fork_user_params
      params.require(:seed_fork_user).permit(:seedfork_user_type)
    end
end
