class ApplicationController < ActionController::Base
  
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  
  protected
  
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:user_name, :type_of_user, :address, :phone, :create_date, :email, :password, :password_confirmation, :remember_me, :seedforkuser_role, :truckdriver_role, :farmer_role, :pantry_role])
    devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password, :remember_me])
    devise_parameter_sanitizer.permit(:account_update, keys: [:user_name, :type_of_user, :address, :phone, :updated_at, :email, :password, :password_confirmation, :remember_me, :type_of_user, :farmer_role, :pantry_role, :truckdriver_role, :seedforkuser_role])
  end
  
end