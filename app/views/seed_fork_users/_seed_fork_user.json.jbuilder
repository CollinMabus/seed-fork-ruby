json.extract! seed_fork_user, :id, :seedfork_user_type, :created_at, :updated_at
json.url seed_fork_user_url(seed_fork_user, format: :json)