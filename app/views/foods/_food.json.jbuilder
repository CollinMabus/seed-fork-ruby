json.extract! food, :id, :food, :amount, :frequency, :duration, :price_cap, :created_at, :updated_at
json.url food_url(food, format: :json)