# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180122200603) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "donors", force: :cascade do |t|
    t.string   "username"
    t.date     "date_entered"
    t.string   "email"
    t.string   "user_type"
    t.integer  "quantity"
    t.string   "address"
    t.string   "season"
    t.string   "match"
    t.date     "pickup_date"
    t.string   "pickup_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "type_of_food"
    t.string   "delivered"
    t.integer  "weight"
    t.integer  "price"
    t.text     "units"
  end

  create_table "foods", force: :cascade do |t|
    t.string   "food"
    t.decimal  "amount"
    t.string   "frequency"
    t.string   "duration"
    t.decimal  "price_cap"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recipients", force: :cascade do |t|
    t.string   "username"
    t.date     "date_entered"
    t.string   "email"
    t.string   "user_type"
    t.integer  "quantity"
    t.string   "address"
    t.string   "match"
    t.date     "dropoff_date"
    t.string   "dropoff_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "type_of_food"
  end

  create_table "seed_fork_users", force: :cascade do |t|
    t.string   "seedfork_user_type"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "user_types", force: :cascade do |t|
    t.string   "user_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "user_name"
    t.string   "type_of_user"
    t.string   "address"
    t.string   "phone"
    t.date     "create_date"
    t.boolean  "seedforkuser_role",      default: false
    t.boolean  "truckdriver_role",       default: false
    t.boolean  "farmer_role",            default: false
    t.boolean  "pantry_role",            default: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
