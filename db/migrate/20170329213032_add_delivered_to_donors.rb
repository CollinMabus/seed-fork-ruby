class AddDeliveredToDonors < ActiveRecord::Migration[5.0]
  def change
    add_column :donors, :delivered, :string
  end
end
