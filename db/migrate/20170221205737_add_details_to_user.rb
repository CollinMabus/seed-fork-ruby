class AddDetailsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :user_name, :string
    add_column :users, :type_of_user, :string
    add_column :users, :address, :string
    add_column :users, :phone, :string
    add_column :users, :create_date, :date
  end
end
