class AddUnitsToDonors < ActiveRecord::Migration[5.0]
  def change
    add_column :donors, :units, :text
  end
end
