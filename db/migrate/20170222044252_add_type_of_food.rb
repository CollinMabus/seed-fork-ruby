class AddTypeOfFood < ActiveRecord::Migration[5.0]
  def change
    add_column :donors, :type_of_food, :string
    add_column :recipients, :type_of_food, :string
  end
end
