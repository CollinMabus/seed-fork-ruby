class CreateSeedForkUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :seed_fork_users do |t|
      t.string :seedfork_user_type

      t.timestamps
    end
  end
end
