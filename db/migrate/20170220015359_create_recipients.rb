class CreateRecipients < ActiveRecord::Migration[5.0]
  def change
    create_table :recipients do |t|
      t.string :username
      t.date :date_entered
      t.string :email
      t.string :user_type
      t.integer :quantity
      t.string :address
      t.string :match
      t.date :dropoff_date
      t.string :dropoff_name

      t.timestamps
    end
  end
end
