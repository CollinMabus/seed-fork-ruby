class AddPriceToDonors < ActiveRecord::Migration[5.0]
  def change
    add_column :donors, :price, :int
  end
end
