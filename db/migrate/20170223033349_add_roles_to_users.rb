class AddRolesToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :seedforkuser_role, :boolean, default: false
    add_column :users, :truckdriver_role, :boolean, default: false
    add_column :users, :farmer_role, :boolean, default: false
    add_column :users, :pantry_role, :boolean, default: false
  end
end
