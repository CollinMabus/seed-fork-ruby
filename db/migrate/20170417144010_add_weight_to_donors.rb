class AddWeightToDonors < ActiveRecord::Migration[5.0]
  def change
    add_column :donors, :weight, :integer
  end
end
