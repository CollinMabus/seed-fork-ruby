class ChangeDataTypeForUserType < ActiveRecord::Migration[5.0]
  def change
    change_column :user_types, :user_type, :string
  end
end
