class CreateFoods < ActiveRecord::Migration[5.0]
  def change
    create_table :foods do |t|
      t.string :food
      t.decimal :amount
      t.string :frequency
      t.string :duration
      t.decimal :price_cap

      t.timestamps
    end
  end
end
