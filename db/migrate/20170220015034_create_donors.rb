class CreateDonors < ActiveRecord::Migration[5.0]
  def change
    create_table :donors do |t|
      t.string :username
      t.date :date_entered
      t.string :email
      t.string :user_type
      t.integer :quantity
      t.string :address
      t.string :season
      t.string :match
      t.date :pickup_date
      t.string :pickup_name

      t.timestamps
    end
  end
end
